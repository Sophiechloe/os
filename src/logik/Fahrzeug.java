package logik;

public class Fahrzeug {
	
	// Attribute 
	private String kennzeichen;
	private double anschaffungskosten;
	private int anschaffungsjahr;
	private int nutzungsdauer;

	// Konstruktor
	public Fahrzeug(String k, double ak, int aj, int nd) {
		this.kennzeichen = k;
		this.anschaffungskosten = ak;
		this.anschaffungsjahr = aj;
		this.nutzungsdauer = nd;
	}


	// get-/set-Methoden
	public String getKennzeichen() {
		return kennzeichen;
	}
	public void setKennzeichen(String kennzeichen) {
		this.kennzeichen = kennzeichen;
	}
	public double getAnschaffungskosten() {
		return anschaffungskosten;
	}
	public void setAnschaffungskosten(double anschaffungskosten) {
		this.anschaffungskosten = anschaffungskosten;
	}
	public int getAnschaffungsjahr() {
		return anschaffungsjahr;
	}
	public void setAnschaffungsjahr(int anschaffungsjahr) {
		this.anschaffungsjahr = anschaffungsjahr;
	}
	public int getNutzungsdauer() {
		return nutzungsdauer;
	}
	public void setNutzungsdauer(int nutzungsdauer) {
		this.nutzungsdauer = nutzungsdauer;
	}
	
	
	public String toString(){
		return this.kennzeichen;
	}

	// eigene Methoden
	public String berechneAbschreibung() {
		int nutzungsdauer = getNutzungsdauer();
		double anschaffungskosten = getAnschaffungskosten();
		int jahr = getAnschaffungsjahr();
		double abschreibung = anschaffungskosten / nutzungsdauer;
		double restwert = anschaffungskosten;
		String ausgabe = "Jahr\tAbschr\tRestwert\n";
		// berechne �ber die Nutzungsdauer hinweg jeweils
		for (int a = 0; a < nutzungsdauer; a++) {
			restwert = restwert - abschreibung;
			ausgabe = ausgabe
					+ (jahr + a) + "\t"
					+ String.format("%.2f", abschreibung) + "\t"
					+ String.format("%.2f", restwert) + "\n";
		}
		return ausgabe;
	}


}
