package gui;

import logik.Fahrzeug;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Hauptfenster extends JFrame implements ListSelectionListener {

	private JPanel contentPane;
	private JTextField txtKennzeichen;
	private JTextField txtAnschaffungskosten;
	private JTextField txtNutzungsdauer;
	private JTextField txtAnschaffungsjahr;
	private JTextArea taAusgabe;
	private JList<Fahrzeug> taListe;

	private DefaultListModel<Fahrzeug> fahrzeugListe = new DefaultListModel<Fahrzeug>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Hauptfenster frame = new Hauptfenster();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Hauptfenster() {

		fahrzeugListe.addElement(new Fahrzeug("MA-PO-42", 20000, 2020, 10));
		fahrzeugListe.addElement(new Fahrzeug("HD-OP-1337", 15000, 2010, 8));
		fahrzeugListe.addElement(new Fahrzeug("SW-II-42", 15000, 2000, 7));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 875, 309);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNeuesFahrzeugAnlegen = new JLabel("Neues Fahrzeug anlegen");
		lblNeuesFahrzeugAnlegen.setBounds(32, 28, 179, 14);
		contentPane.add(lblNeuesFahrzeugAnlegen);
		
		txtKennzeichen = new JTextField();
		txtKennzeichen.setBounds(157, 50, 157, 20);
		contentPane.add(txtKennzeichen);
		txtKennzeichen.setColumns(10);
		
		JLabel lblKennzeichen = new JLabel("Kennzeichen");
		lblKennzeichen.setBounds(32, 56, 86, 14);
		contentPane.add(lblKennzeichen);
		
		JLabel lblWert = new JLabel("Anschaffungskosten");
		lblWert.setBounds(32, 87, 115, 14);
		contentPane.add(lblWert);
		
		txtAnschaffungskosten = new JTextField();
		txtAnschaffungskosten.setBounds(157, 81, 157, 20);
		contentPane.add(txtAnschaffungskosten);
		txtAnschaffungskosten.setColumns(10);
		
		txtNutzungsdauer = new JTextField();
		txtNutzungsdauer.setBounds(157, 112, 157, 20);
		contentPane.add(txtNutzungsdauer);
		txtNutzungsdauer.setColumns(10);
		
		JLabel lblNutzungsdauer = new JLabel("Nutzungsdauer");
		lblNutzungsdauer.setBounds(32, 118, 86, 14);
		contentPane.add(lblNutzungsdauer);
		
		JButton btnAnlegen = new JButton("Neu");
		btnAnlegen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				felderZuruecksetzen();
			}
		});
		btnAnlegen.setBounds(29, 193, 89, 23);
		contentPane.add(btnAnlegen);
		
		JButton btnLschen = new JButton("L\u00F6schen");
		btnLschen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int idx = taListe.getSelectedIndex();
				if (idx >= 0) {
					fahrzeugListe.remove(idx);
				}

			}
		});
		btnLschen.setBounds(126, 193, 85, 23);
		contentPane.add(btnLschen);
		
		JButton btnAbschreibungen = new JButton("Abschreibungen");
		btnAbschreibungen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// txtKennzeichen auslesen
				Fahrzeug fzg = taListe.getSelectedValue();
				if (fzg != null) {
					String ausgabe = fzg.berechneAbschreibung();
					taAusgabe.setText(ausgabe);
				} else {
					taAusgabe.setText("");
				}
			}
		});
		btnAbschreibungen.setBounds(32, 227, 282, 23);
		contentPane.add(btnAbschreibungen);
		
		txtAnschaffungsjahr = new JTextField();
		txtAnschaffungsjahr.setBounds(157, 145, 157, 20);
		contentPane.add(txtAnschaffungsjahr);
		txtAnschaffungsjahr.setColumns(10);
		
		JLabel lblAnschaffungsjahr = new JLabel("Anschaffungsjahr");
		lblAnschaffungsjahr.setBounds(32, 148, 115, 14);
		contentPane.add(lblAnschaffungsjahr);
		
		taAusgabe = new JTextArea();
		taAusgabe.setBounds(620, 48, 233, 204);
		taListe = new JList<Fahrzeug>();
		taListe.setModel(fahrzeugListe);
		taListe.setBounds(372, 48, 233, 204);
		contentPane.add(taListe );
		contentPane.add(taAusgabe);

		taListe.addListSelectionListener(this);
		
		JButton btnAnzeigen = new JButton("Speichern");
		btnAnzeigen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// Werte aus den Textfeldern auslesen
				String kennzeichen = txtKennzeichen.getText();
				double anschaffungskosten = Double.parseDouble(txtAnschaffungskosten.getText());
				int anschaffungsjahr = Integer.parseInt(txtAnschaffungsjahr.getText());
				int nutzungsdauer = Integer.parseInt(txtNutzungsdauer.getText());

				// Objekt vom Typ Fahrzeug erzeugen (mit Konstruktor)
				Fahrzeug fz = new Fahrzeug(kennzeichen, anschaffungskosten, anschaffungsjahr, nutzungsdauer);

				// Objekt der ArrayList fahrzeuge hinzufuegen.
				fahrzeugListe.addElement(fz);
			}
		});
		btnAnzeigen.setBounds(225, 193, 89, 23);
		contentPane.add(btnAnzeigen);
	}


	@Override
	public void valueChanged(ListSelectionEvent e) {
		Fahrzeug fzg = taListe.getSelectedValue();
		if (fzg != null) {
			txtKennzeichen.setText(fzg.getKennzeichen());
			txtAnschaffungsjahr.setText(Integer.toString(fzg.getAnschaffungsjahr()));
			txtAnschaffungskosten.setText(Double.toString(fzg.getAnschaffungskosten()));
			txtNutzungsdauer.setText(Integer.toString(fzg.getNutzungsdauer()));
		} else {
			felderZuruecksetzen();
		}
	}

	public void felderZuruecksetzen() {
		txtAnschaffungsjahr.setText("");
		txtKennzeichen.setText("");
		txtAnschaffungskosten.setText("");
		txtNutzungsdauer.setText("");
	}
}
